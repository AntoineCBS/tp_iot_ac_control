# aws_iot_certificate cert
resource "aws_iot_certificate" "AC_LAB_RC_cert" {
  active = true
}

# aws_iot_policy pub-sub
resource "aws_iot_policy" "AC_LAB_RC_policy" {
  name = "AC_LAB_RC_TP_policy"
  policy = file("files/iot_policy.json")
}

# aws_iot_policy_attachment attachment
resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.AC_LAB_RC_policy.name
  target = aws_iot_certificate.AC_LAB_RC_cert.arn
}

# aws_iot_thing temp_sensor
resource "aws_iot_thing" "AC_LAB_RC_temp_sensor" {
  name = "temp_sensor"

  attributes = {
    First = "temperature"
  }
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "AC_LAB_RC_att" {
  principal = aws_iot_certificate.AC_LAB_RC_cert.arn
  thing     = aws_iot_thing.AC_LAB_RC_temp_sensor.name
}

# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "iot_endpoint" {
  endpoint_type = "iot:Data-ATS"
}

# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "AC_LAB_RC_rule" {
  name        = "AC_LAB_RC_rule"
  description = "Rule used for the TP"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+' WHERE temperature >= 40"
  sql_version = "2016-03-23"

  dynamodbv2 {
    role_arn = aws_iam_role.iot_role.arn
    put_item {
      table_name = aws_dynamodb_table.AC_LAB_RC_dynamo_table.name
    }
  }
}
# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "rule" {
  name        = "MyRule"
  description = "Example rule"
  enabled     = true
  sql         = "SELECT * FROM 'sensor/temperature/+'"
  sql_version = "2016-03-23"

  timestream{
    database_name = aws_timestreamwrite_database.AC_LAB_RC_timestream_database.database_name
    role_arn = aws_iam_role.iot_role.arn
    table_name = aws_timestreamwrite_table.AC_LAB_RC_timestream_table.table_name

    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }

  }
}

###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
