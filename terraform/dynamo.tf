# aws_dynamodb_table
resource "aws_dynamodb_table" "AC_LAB_RC_dynamo_table" {
  name           = "temperature"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }

}