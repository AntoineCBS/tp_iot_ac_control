# aws_timestreamwrite_database
resource "aws_timestreamwrite_database" "AC_LAB_RC_timestream_database" {
  database_name = "iot"
}
# aws_timestreamwrite_table linked to the database
resource "aws_timestreamwrite_table" "AC_LAB_RC_timestream_table" {
  database_name = aws_timestreamwrite_database.AC_LAB_RC_timestream_database.database_name
  table_name    = "temperaturesensor"
}